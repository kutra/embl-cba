## December 2019

Tischi's Mac to Desktop (SSD):
100 files in 270 ms.

Tischi's Mac to /g/cba (with USB-C LAN):
100 files in 3070 ms.

Tischi's Mac to /g/cba (with Wireless):
100 files in 3200 ms.

ALMF VM Windows Sliver to /g/cba: 
100 files in 3700 ms.

ALMF VM Windows Sliver to Desktop: 
100 files in 600 ms.

ALMF VM Ubuntu to /g/cba: 
100 files in 680 ms.

ALMF VM Ubuntu to Desktop: 
100 files in 200 ms.

Interactive cluster node on x2goclient to /g/cba:
100 files in 750 ms.


