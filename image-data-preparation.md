# Image data preparation

This document contains recommendations of how to organise your image data, e.g. when seeking consultancy at EMBL-CBA.

## Select a minimal set of representative examples

Select **two** (not more) data sets that clearly show what you want to measure. 
Ideally, the two data sets show a clear difference in the feature that you would like  to measure.
For example the first data set could be a control sample, whereas the second data set has been treated in some way.

Organise those two data sets into a folder structure like this:

- minimal-example-data
    - control
        - ... (image data)
    - treatment01
        - ... (image data)

Optionally, you may prepare additional data sets, by creating an additional folder:

- additional-example-data
    - control
        - ... (image data)
        - ... (more image data)
    - treatment01
        - ... (image data here)
    - treatment02
        - ...
  
## Sharing your image data set

If the selected minimal data are less than few tens of GB, please copy them onto the `\cba\exchange` network share.

Everybody has access with their normal EMBL user account.

### How to mount `/cba/exchange`

- Windows: Mapping `\\cba\cba\exchange`
    - Make sure you are in the EMBL intranet 
    - Use "Map network drive" within "Windows Explorer" and enter
        - \\\\cba\cba\exchange
    - you can log in with your normal embl account:
        - embl\USERNAME
        - PASSWORD
- MacOS: Mapping `/Volumes/cba/exchange`
    - Make sure you are in the EMBL intranet 
    - Within Finder, "Go > Connect to Server" and enter
    - `cifs://USERNAME:*@cba.embl.de/cba/exchange`
    - replacing `USERNAME` by your embl username
    - In Finder, you can now "Go > Go to Folder" and enter `/Volumes/cba/exchange`
- Linux: Mapping `/g/cba/exchange`
    - Open a terminal window and install cifs (e.g. apt-get install cifs-utils)
    - mkdir -p ~/MYLOCALMOUNTPOINT
    - sudo mount -t cifs -o uid=$(id -u),gid=$(id -g),username=USERNAME //cba.embl.de/cba/exchange ~/MYLOCALMOUNTPOINT
    - replacing `USERNAME` by your embl username
    - replacing `MYLOCALMOUNTPOINT` by a folder name that you like

After you mounted `/cba/exchange` you may create a folder there with your name simply copy the data.



  
  

